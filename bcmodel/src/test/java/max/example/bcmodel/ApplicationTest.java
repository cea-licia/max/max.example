/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import max.core.ExperimenterAction;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActivationScheduleFactory;
import max.datatype.ledger.Transaction;
import max.example.bcmodel.action.BCAgent;
import max.example.bcmodel.env.BCContext;
import max.example.bcmodel.env.BCEnvironment;
import max.example.bcmodel.exp.BCExperimenter;
import max.example.bcmodel.role.RBCMiner;
import max.model.ledger.blockchain.action.BlockCreationOracle;
import max.model.ledger.blockchain.selection.MembershipSelectionStrategyFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/** Test the application against various scenarios. */
public class ApplicationTest {

  /** Reset MAX parameters at each test. */
  @BeforeEach
  public void before(@TempDir Path tempDir) {
    clearParameters();
    setParameter("MAX_CORE_SIMULATION_STEP", "1");
    setParameter("MAX_CORE_UI_MODE", "Silent");
    setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());

    setParameter("MAX_CORE_MAX_OUTBOUND_CONNECTIONS", "10");
    setParameter("MAX_CORE_MAX_INBOUND_CONNECTIONS", "10");
    setParameter("MAX_MODEL_NETWORK_DELAY", "1");
    setParameter("MAX_MODEL_NETWORK_RELIABILITY", "1");

    setParameter("MAX_MODEL_LEDGER_FEE", "0.0");
    setParameter("MAX_MODEL_LEDGER_MAX_NUMBER_TXS", "4000");
    setParameter("MAX_MODEL_LEDGER_BLOCK_CREATION_RATE", "5");
  }

  /**
   * Test all messages:
   *
   * <ul>
   *   <li>Issue transactions to test TX messages
   *   <li>Create blocks to test BLOCK messages
   *   <li>Make a user join the simulation later, so it will need to issue some GET_BLOCK messages
   * </ul>
   *
   * @param testInfo Injected by JUnit5
   */
  @Test
  public void messagingTest(TestInfo testInfo) throws Throwable {
    final var experimenter =
        new BCExperimenter() {
          private BCEnvironment env;
          private BCAgent<Transaction> miner;
          private BCAgent<Transaction> user1;
          private BCAgent<Transaction> user2;

          private double user1Balance;
          private double user2Balance;

          @Override
          protected List<MAXAgent> setupScenario() {
            env = new BCEnvironment();
            BlockCreationOracle oracle =
                new BlockCreationOracle(
                    env.getName(),
                    ActivationScheduleFactory.createOneTime(BigDecimal.TEN),
                    RBCMiner.class,
                    MembershipSelectionStrategyFactory.createUniformSingleWinnerPoWStrategy(
                        env.getName()));

            miner = createMiner(env.getName());
            user1 = createUser(env.getName());

            return Arrays.asList(env, oracle, miner, user1);
          }

          @Override
          protected void setupExperimenter() {
            // Schedule an action to create user2
            schedule(
                new ExperimenterAction<>(this) {
                  @Override
                  public void execute() {
                    user2 = createUser(env.getName());
                    launchAgents(Collections.singletonList(user2));
                  }
                }.oneTime(35));

            schedule(
                new ExperimenterAction<>(this) {
                  @Override
                  public void execute() {
                    final var ctx1 = (BCContext<Transaction>) user1.getContext(env.getName());
                    final var ctx2 = (BCContext<Transaction>) user2.getContext(env.getName());

                    user1Balance = ctx1.getBalance();
                    user2Balance = ctx2.getBalance();
                  }
                }.oneTime(40));

            // Schedule an action to test properties of the system
            schedule(
                new ExperimenterAction<>(this) {
                  @Override
                  public void execute() {
                    final var ctx0 = (BCContext<Transaction>) miner.getContext(env.getName());
                    final var ctx1 = (BCContext<Transaction>) user1.getContext(env.getName());
                    final var ctx2 = (BCContext<Transaction>) user2.getContext(env.getName());

                    // At least one block created ?
                    Assertions.assertTrue(ctx0.getBlockTree().size() > 0);

                    // Make sure they have the same blockchain (same size and same blocks)
                    Assertions.assertEquals(ctx0.getBlockTree().size(), ctx1.getBlockTree().size());
                    Assertions.assertEquals(ctx0.getBlockTree().size(), ctx2.getBlockTree().size());

                    for (final var block : ctx0.getBlockTree()) {
                      Assertions.assertTrue(ctx1.getBlockTree().contains(block.hashCode()));
                      Assertions.assertTrue(ctx2.getBlockTree().contains(block.hashCode()));
                    }

                    // Transactions issued ?
                    Assertions.assertTrue(ctx1.getBalance() < user1Balance);
                    Assertions.assertTrue(ctx2.getBalance() < user2Balance);
                  }
                }.oneTime(95));
          }
        };

    launchTester(experimenter, 100, testInfo);
  }
}
