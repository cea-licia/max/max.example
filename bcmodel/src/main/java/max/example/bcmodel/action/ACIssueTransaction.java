/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.action;

import java.util.Random;
import java.util.logging.Level;
import max.core.action.Action;
import max.datatype.com.UUIDAddress;
import max.datatype.ledger.Payload;
import max.datatype.ledger.Transaction;
import max.example.bcmodel.env.BCContext;
import max.example.bcmodel.env.message.MessageFactory;
import max.example.bcmodel.role.RBCUser;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * Action to issue a random transaction.
 *
 * <ul>
 *   <li>Payload: value between 1 and 100 (inclusive)
 *   <li>Fees: 0
 *   <li>Receiver: random UUID address
 * </ul>
 *
 * @max.role {@link RBCUser}
 * @param <T> Transaction type
 */
public class ACIssueTransaction<T extends Transaction> extends Action<BCAgent<T>> {

  // Random number generator
  private static final Random RANDOM_GEN = new Random();

  /**
   * Create a new {@link ACCreateBCBlock} instance.
   *
   * @param environment where this action will be executed
   * @param owner action's owner
   */
  public ACIssueTransaction(String environment, BCAgent<T> owner) {
    super(environment, RBCUser.class, owner);
  }

  @Override
  public void execute() {
    // Get context
    final var ctx = (BCContext<T>) getOwner().getContext(getEnvironment());

    // Create a random address
    final var receiver = new UUIDAddress();

    // Issue transaction
    final var amount = Math.min(ctx.getBalance(), 1d + RANDOM_GEN.nextInt(100));
    if (amount > 0) {
      final var tx =
          ctx.getTransactionFactory()
              .createTransaction(ctx.getAddress(), receiver, 0d, new Payload(amount));
      ((P2PEnvironment) ctx.getEnvironment())
          .sendMessage(
              MessageFactory.createTXMessage(
                  ctx.getMyAddress(RNetworkPeer.class.getName()), ctx.getNeighborsAddresses(), tx));
      ctx.setBalance(ctx.getBalance() - amount);
      getOwner().getLogger().log(Level.INFO, "Issuing a transaction: " + amount + "BC");
    }
  }

  @Override
  public <T0 extends Action<BCAgent<T>>> T0 copy() {
    return (T0) new ACIssueTransaction<>(getEnvironment(), getOwner());
  }
}
