/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.action;

import max.core.action.Plan;
import max.datatype.ledger.Transaction;
import max.example.bcmodel.role.RBCMiner;
import max.example.bcmodel.role.RBCUser;
import max.model.ledger.blockchain.action.BlockchainAgent;

/**
 * A very simple blockchain agent.
 *
 * <p>Basically a {@link BlockchainAgent} that can play {@link RBCUser} and {@link RBCMiner} roles.
 *
 * @param <T> Transaction type
 */
public class BCAgent<T extends Transaction> extends BlockchainAgent<T> {

  /**
   * Default constructor.
   *
   * @param plan plan to be executed by the agent
   */
  public BCAgent(Plan<? extends BlockchainAgent<T>> plan) {
    super(plan);

    // Add our custom roles to the list of playable roles
    addPlayableRole(RBCMiner.class);
    addPlayableRole(RBCUser.class);
  }
}
