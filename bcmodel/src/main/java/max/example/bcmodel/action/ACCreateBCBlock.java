/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.action;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import max.core.action.Action;
import max.datatype.ledger.Transaction;
import max.datatype.ledger.blockchain.Block;
import max.example.bcmodel.env.BCContext;
import max.example.bcmodel.env.message.MessageFactory;
import max.example.bcmodel.role.RBCMiner;
import max.model.ledger.blockchain.action.ACCreateBlock;
import max.model.ledger.blockchain.action.BlockCreationOracle;
import max.model.ledger.blockchain.role.RBlockCreationOracle;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * Action executed when the agent must create a new BC block.
 *
 * @max.role {@link RBCMiner}
 * @param <T> Transaction type
 */
public class ACCreateBCBlock<T extends Transaction> extends ACCreateBlock<BCAgent<T>> {

  // Store oracle instance
  private BlockCreationOracle oracle;

  /**
   * Default constructor.
   *
   * @param environment Environment in which the action will be executed
   * @param owner Action's owner
   */
  public ACCreateBCBlock(String environment, BCAgent<T> owner) {
    super(environment, RBCMiner.class, owner);
  }

  @Override
  public void execute() {
    // Get context, as usual
    final var ctx = (BCContext<T>) getOwner().getContext(getEnvironment());

    // Get block number getBlockNumber() - 1: it is the parent block
    final var parent = ctx.getBlockTree().getBlockByNumber(getBlockNumber() - 1);

    // Select transactions
    final var selectedTX =
        ctx.getMemoryPool().values().stream()
            .takeWhile(
                new Predicate<T>() {

                  private int currentCount;

                  @Override
                  public boolean test(T t) {
                    if (currentCount < ctx.getMaxNumberOfTxs()) {
                      currentCount += 1;
                      return true;
                    }
                    return false;
                  }
                })
            .collect(Collectors.toList());

    // Create block
    final var block =
        new Block<>(
            getBlockNumber(), parent.hashCode(), getOwner().getCurrentTick());
    block.addTransactions(new ArrayList<>(selectedTX));

    // Send block
    final var me = ctx.getMyAddress(RNetworkPeer.class.getName());
    final var receivers = ctx.getNeighborsAddresses();
    receivers.add(me); // So I can add the block to my blockchain
    ((P2PEnvironment) ctx.getEnvironment())
        .sendMessage(MessageFactory.createBlockMessage(me, receivers, block));

    // Increase height
    if (oracle == null) {
      oracle =
          (BlockCreationOracle)
              getOwner()
                  .getAgentsWithRole(getEnvironment(), RBlockCreationOracle.class)
                  .get(0)
                  .getAgent();
    }
    oracle.setHeight(getBlockNumber());
  }

  @Override
  public <T0 extends Action<BCAgent<T>>> T0 copy() {
    return (T0) new ACCreateBCBlock<>(getEnvironment(), getOwner());
  }
}
