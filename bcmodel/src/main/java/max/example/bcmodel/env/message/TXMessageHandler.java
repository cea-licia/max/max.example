/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.env.message;

import java.util.logging.Level;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Transaction;
import max.example.bcmodel.env.BCContext;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageHandler;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * A message handler dedicated to 'TX' messages.
 *
 * <p>When executed, it will add the transaction to the agent's {@link
 * max.datatype.ledger.blockchain.MemoryPool} and broadcast the message.
 */
public class TXMessageHandler implements MessageHandler {

  /** Message type this handler can handle. */
  public static final String MESSAGE_TYPE = MessageFactory.TX_MSG_TYPE;

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    p2PContext
        .getOwner()
        .getLogger()
        .log(Level.INFO, "Received TX from " + message.getSender().getAgent().getName());

    // The context is a BCContext, so we can safely cast
    final var ctx = (BCContext<Transaction>) p2PContext;

    // Get the transaction
    final var tx = (Transaction) message.getPayload();

    // Add the transaction to the memory pool of the agent if not already added
    // And broadcast the message to neighbors.
    final var memoryPool = ctx.getMemoryPool();
    if (!memoryPool.containsHashcode(tx.hashCode())) {
      // Add to the memory pool
      memoryPool.put(tx.hashCode(), tx);

      // Propagate transaction to neighbors
      final var neighbors = ctx.getNeighborsAddresses();
      neighbors.remove(message.getSender());
      ((P2PEnvironment) ctx.getEnvironment())
          .sendMessage(
              MessageFactory.createTXMessage(
                  ctx.getMyAddress(RNetworkPeer.class.getName()), neighbors, tx));
    }
  }
}
