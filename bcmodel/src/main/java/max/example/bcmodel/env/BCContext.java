/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.env;

import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.datatype.com.Address;
import max.datatype.com.UUIDAddress;
import max.datatype.ledger.Transaction;
import max.model.ledger.blockchain.env.BlockchainContext;

/**
 * Context for blockchain nodes.
 *
 * <p>Simply defines a default address in the wallet.
 *
 * @param <T> Transaction type
 */
public class BCContext<T extends Transaction> extends BlockchainContext<T> {

  // The key to retrieve our BC Address in our Wallet
  private static final String BC_ADDRESS_KEY = "BC";

  /**
   * Create a new {@link BCContext}.
   *
   * @param owner Context's owner
   * @param simulatedEnvironment Associated environment
   */
  public BCContext(SimulatedAgent owner, SimulatedEnvironment simulatedEnvironment) {
    super(owner, simulatedEnvironment);

    // Add a new address in the Wallet
    getWallet().addAddress(BC_ADDRESS_KEY, new UUIDAddress());
  }

  @Override
  public Address getAddress() {
    return getWallet().getAddress(BC_ADDRESS_KEY);
  }
}
