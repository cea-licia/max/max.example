/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.env.message;

import java.util.List;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Transaction;
import max.datatype.ledger.blockchain.Block;

/**
 * Factory to ease the creation of our messages.
 *
 * Defines 3 types of messages:
 * <ul>
 *     <li>TX that carries a transaction</li>
 *     <li>BLOCK that carries a block</li>
 *     <li>GET_BLOCK that carries a block hash</li>
 * </ul>
 */
public final class MessageFactory {

    // All messages for the protocol
    public static final String TX_MSG_TYPE = "TX";
    public static final String BLOCK_MSG_TYPE = "BLOCK";
    public static final String GET_BLOCK_MSG_TYPE = "GET_BLOCK";

    /**
     * Create a new TX message.
     *
     * @param sender Address of the sender
     * @param receivers A non-empty list of Addresses
     * @param tx The transaction to propagate
     * @return A message containing the transaction
     */
    public static Message<AgentAddress, Transaction> createTXMessage(AgentAddress sender, List<AgentAddress> receivers, Transaction tx) {
        final var msg = new Message<>(sender, receivers, tx);
        msg.setType(TX_MSG_TYPE);
        return msg;
    }

    /**
     * Create a new BLOCK message.
     *
     * @param sender Address of the sender
     * @param receivers A non-empty list of Addresses
     * @param block The block to propagate
     * @return A message containing the block
     */
    public static <T0 extends Transaction, T1 extends Number> Message<AgentAddress, Block<T0, T1>> createBlockMessage(AgentAddress sender, List<AgentAddress> receivers, Block<T0, T1> block) {
        final var msg = new Message<>(sender, receivers, block);
        msg.setType(BLOCK_MSG_TYPE);
        return msg;
    }

    /**
     * Create a new GET_BLOCK message.
     *
     * @param sender Address of the sender
     * @param receivers A non-empty list of Addresses
     * @param blockHash The hash of the block the sender is requesting
     * @return A message containing the hash value
     */
    public static Message<AgentAddress, Integer> createGetBlockMessage(AgentAddress sender, List<AgentAddress> receivers, Integer blockHash) {
        final var msg = new Message<>(sender, receivers, blockHash);
        msg.setType(GET_BLOCK_MSG_TYPE);
        return msg;
    }

    /**
     * Private empty constructor to prevent instantiation.
     */
    private MessageFactory() {
        // Empty constructor, prevent instantiation
    }

}
