/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.example.bcmodel.role.RBCMiner;
import max.example.bcmodel.role.RBCUser;
import max.model.ledger.blockchain.env.BlockchainEnvironment;

/**
 * The {@link BCEnvironment} class represents the environment in which blockchain nodes will evolve.
 *
 * <p>It is basically a {@link BlockchainEnvironment} that allows our roles {@link RBCUser} and
 * {@link RBCMiner}. Created contexts are instances of {@link BCContext}.
 */
public class BCEnvironment extends BlockchainEnvironment {

  /** Default Constructor. */
  public BCEnvironment() {
    allowsRole(RBCMiner.class);
    allowsRole(RBCUser.class);
  }

  /**
   * {@inheritDoc}
   *
   * @return an instance of {@link BCContext}
   */
  @Override
  protected Context createContext(SimulatedAgent agent) {
    return new BCContext<>(agent, this);
  }
}
