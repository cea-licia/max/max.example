/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.env.message;

import java.util.Collections;
import java.util.logging.Level;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Transaction;
import max.example.bcmodel.env.BCContext;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageHandler;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * {@link MessageHandler} dedicated to 'GET_BLOCK' messages.
 *
 * <p>It will search a block in the local blockchain copy matching the received hash-code. If found,
 * then this handler sends a 'BLOCK' message containing the block.
 */
public class GETBLOCKMessageHandler implements MessageHandler {

  /** Message type this handler can handle. */
  public static final String MESSAGE_TYPE = MessageFactory.GET_BLOCK_MSG_TYPE;

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    p2PContext
        .getOwner()
        .getLogger()
        .log(Level.INFO, "Received GET_BLOCK from " + message.getSender().getAgent().getName());

    // The context is a BCContext, so we can safely cast
    final var ctx = (BCContext<Transaction>) p2PContext;

    // Get the transaction
    final var hashCode = (Integer) message.getPayload();

    // Search in our local blockchain copy the block
    final var block = ctx.getBlockTree().getBlockByHashcode(hashCode);

    // Send the block if found
    if (block != null) {
      ((P2PEnvironment) p2PContext.getEnvironment())
          .sendMessage(
              MessageFactory.createBlockMessage(
                  ctx.getMyAddress(RNetworkPeer.class.getName()),
                  Collections.singletonList(message.getSender()),
                  block));
    }
  }
}
