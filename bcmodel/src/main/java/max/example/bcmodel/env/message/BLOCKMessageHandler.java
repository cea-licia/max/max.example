/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.env.message;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Transaction;
import max.datatype.ledger.blockchain.Block;
import max.example.bcmodel.env.BCContext;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageHandler;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * Specialized {@link MessageHandler} for handling `BLOCK` messages.
 *
 * <p>It will attempt to add the block to the local blockchain and if it succeeds, update the
 * agent's balance. This handler is also aware of Orphan blocks and will try to attach them if
 * possible.
 *
 * <p>If the received block is an orphan one, it will send a `GET_BLOCK` message to the neighbors.
 */
public class BLOCKMessageHandler implements MessageHandler {

  /** Message type this handler can handle. */
  public static final String MESSAGE_TYPE = MessageFactory.BLOCK_MSG_TYPE;

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    p2PContext
        .getOwner()
        .getLogger()
        .log(Level.INFO, "Received BLOCK from " + message.getSender().getAgent().getName());

    // The context is a BCContext, so we can safely cast
    final var ctx = (BCContext<Transaction>) p2PContext;

    // Get the transaction
    final var block = (Block<Transaction, BigDecimal>) message.getPayload();

    // Add block and propagate only if we don't already have it
    if (!ctx.getBlockTree().contains(block.hashCode())
        && !ctx.getOrphanBlockList().contains(block)) {
      addBlockToBC(ctx, block);
      final var neighbors = new ArrayList<>(ctx.getNeighborsAddresses());
      neighbors.remove(message.getSender());
      ((P2PEnvironment) ctx.getEnvironment())
          .sendMessage(
              MessageFactory.createBlockMessage(
                  ctx.getMyAddress(RNetworkPeer.class.getName()), neighbors, block));
    }
  }

  /**
   * Try to add the block into the local blockchain copy.
   *
   * @param ctx Context
   * @param block Block to add
   */
  private void addBlockToBC(
      final BCContext<Transaction> ctx, final Block<Transaction, BigDecimal> block) {
    // Retrieve blockchain
    final var blockchain = ctx.getBlockTree();

    // Get last block number in our blockchain
    final var lastBlockNumber =
        blockchain.getAllHeadBlocks().stream()
            .max(Comparator.comparingInt(Block::getBlockNumber))
            .orElseThrow()
            .getBlockNumber();

    // Orphan block or not ? Depends if num(block) <= lastBlockNumber + 1
    final var blockNumber = block.getBlockNumber();
    if (blockNumber <= (lastBlockNumber + 1)) {
      // Not orphan, add it to the blockchain
      try {
        blockchain.append(block);
        ctx.getOwner()
            .getLogger()
            .log(
                Level.INFO,
                String.format("Added block nb %s in local blockchain.", block.getBlockNumber()));

        // Try to resolve some orphans
        reconnectOrphans(ctx, block.hashCode());

        // Update our balance
        updateBalance(ctx, block);

      } catch (final IllegalArgumentException e) {
        ctx.getOwner()
            .getLogger()
            .log(
                Level.WARNING,
                String.format("Unable to append block nb %s", block.getBlockNumber()),
                e);
      }
    } else if (!ctx.getOrphanBlockList().contains(block)) {
      // Orphan block, store it and ask parent to friends
      ctx.getOrphanBlockList().add(block);
      ((P2PEnvironment) ctx.getEnvironment())
          .sendMessage(
              MessageFactory.createGetBlockMessage(
                  ctx.getMyAddress(RNetworkPeer.class.getName()),
                  ctx.getNeighborsAddresses(),
                  block.getPreviousBlockHashCode()));
    }
  }

  /**
   * Given a {@link Block} and a {@link BCContext}, update the context balance with transactions
   * made to the agent's address.
   *
   * @param ctx Context
   * @param block Block containing the transactions
   */
  private void updateBalance(BCContext<Transaction> ctx, Block<Transaction, ?> block) {
    block.getTransactions().stream()
        .filter(t -> t.getReceiver().equals(ctx.getAddress()))
        .forEach(t -> ctx.setBalance(ctx.getBalance() + t.getPayload().getNumericalValue()));
  }

  /**
   * Given a {@link BCContext}, try to reconnect stored orphan blocks to the blockchain. Uses the
   * {@link #addBlockToBC(BCContext, Block)} to re-attach blocks.
   *
   * @param ctx Context
   * @param blockHash hash of the block we just added
   */
  private void reconnectOrphans(BCContext<Transaction> ctx, int blockHash) {
    final var orphanList = new ArrayList<>(ctx.getOrphanBlockList());
    for (final var orphan : orphanList) {
      if (orphan.getPreviousBlockHashCode() == blockHash) {
        addBlockToBC(ctx, orphan);
        ctx.getOrphanBlockList().remove(orphan);
      }
    }
  }
}
