/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.exp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import max.core.EnvironmentEvent;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.action.ReactiveAction;
import max.core.agent.ExperimenterAgent;
import max.core.scheduling.ActionActivator;
import max.datatype.ledger.Transaction;
import max.example.bcmodel.action.ACCreateBCBlock;
import max.example.bcmodel.action.ACIssueTransaction;
import max.example.bcmodel.action.BCAgent;
import max.example.bcmodel.env.message.BLOCKMessageHandler;
import max.example.bcmodel.env.message.GETBLOCKMessageHandler;
import max.example.bcmodel.env.message.TXMessageHandler;
import max.example.bcmodel.role.RBCMiner;
import max.example.bcmodel.role.RBCUser;
import max.model.ledger.blockchain.DoubleTransactionPayloadFactory;
import max.model.ledger.blockchain.action.ACSetTransactionFactory;
import max.model.ledger.blockchain.env.BlockCreatorEvent;
import max.model.network.p2p.action.ACCleanupConnections;
import max.model.network.p2p.action.ACCleanupWaitingConnections;
import max.model.network.p2p.action.ACDiscoverNeighbors;
import max.model.network.p2p.action.ACHandleMessages;
import max.model.network.p2p.env.message.MessageEvent;

/**
 * Base experimenter.
 *
 * <p>Defines some useful methods to create users and miners.
 */
public abstract class BCExperimenter extends ExperimenterAgent {

  /**
   * Create and return a new {@link BCAgent}, which will behave like a BC miner.
   *
   * @param environment the environment this agent will join
   * @return the agent instance. Not launched.
   */
  protected BCAgent<Transaction> createMiner(String environment) {
    return new BCAgent<>(getPlan(environment, true));
  }

  /**
   * Create and return a new {@link BCAgent}, which will behave like a BC user.
   *
   * @param environment the environment this agent will join
   * @return the agent instance. Not launched.
   */
  protected BCAgent<Transaction> createUser(String environment) {
    return new BCAgent<>(getPlan(environment, false));
  }

  /**
   * Get an agent plan built using the provided environment.
   *
   * <p>Uses {@link #getStaticPlanPart(String, boolean)} and {@link #getEventBoundPart(String,
   * boolean)} to create the plan.
   *
   * @param env environment the agent will join
   * @param miner is the agent a miner ?
   * @return the plan.
   */
  protected Plan<BCAgent<Transaction>> getPlan(String env, boolean miner) {
    return new Plan<>() {
      @Override
      public List<ActionActivator<BCAgent<Transaction>>> getInitialPlan() {
        return getStaticPlanPart(env, miner);
      }

      @Override
      public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<BCAgent<Transaction>>>>
          getEventBoundActions() {
        return getEventBoundPart(env, miner);
      }
    };
  }

  /**
   * Build the static part of the agent plan.
   *
   * @param env environment to join
   * @param miner is the agent a miner ?
   * @return the static part of the plan
   * @see #getPlan(String, boolean)
   */
  protected List<ActionActivator<BCAgent<Transaction>>> getStaticPlanPart(
      String env, boolean miner) {
    final List<ActionActivator<BCAgent<Transaction>>> initialPlan = new ArrayList<>();
    final var time = getSimulationTime().getCurrentTick();

    // Take the role in the environment
    initialPlan.add(
        new ACTakeRole<BCAgent<Transaction>>(env, (miner) ? RBCMiner.class : RBCUser.class, null)
            .oneTime(time));

    // Schedule the P2P-related actions
    initialPlan.add(
        new ACDiscoverNeighbors<BCAgent<Transaction>>(env, null)
            .repeatInfinitely(time, BigDecimal.ONE));
    initialPlan.add(
        new ACCleanupConnections<BCAgent<Transaction>>(env, null)
            .repeatInfinitely(time, BigDecimal.ONE));
    initialPlan.add(
        new ACCleanupWaitingConnections<BCAgent<Transaction>>(env, null)
            .repeatInfinitely(time, BigDecimal.ONE));

    // BCUser-related actions
    if (!miner) {
      initialPlan.add(
          new ACSetTransactionFactory<Transaction, BCAgent<Transaction>>(
                  env, new DoubleTransactionPayloadFactory(), null)
              .oneTime(time));
      initialPlan.add(
          new ACIssueTransaction<>(env, null).repeatInfinitely(time, BigDecimal.valueOf(10)));
    }
    return initialPlan;
  }

  /**
   * Get the dynamic part of the plan (event-bound actions).
   *
   * @param env Environment where actions are executed
   * @return the event-bound part of the plan
   * @see #getPlan(String, boolean)
   */
  protected Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<BCAgent<Transaction>>>>
      getEventBoundPart(String env, boolean miner) {
    final ACHandleMessages<BCAgent<Transaction>> handleMsgAction =
        new ACHandleMessages<>(env, null);
    handleMsgAction.registerMessageType(TXMessageHandler.MESSAGE_TYPE, new TXMessageHandler());
    handleMsgAction.registerMessageType(
        BLOCKMessageHandler.MESSAGE_TYPE, new BLOCKMessageHandler());
    handleMsgAction.registerMessageType(
        GETBLOCKMessageHandler.MESSAGE_TYPE, new GETBLOCKMessageHandler());

    final Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<BCAgent<Transaction>>>>
        result = new HashMap<>();
    result.put(MessageEvent.class, Collections.singletonList(handleMsgAction));
    if (miner) {
      result.put(
          BlockCreatorEvent.class, Collections.singletonList(new ACCreateBCBlock<>(env, null)));
    }

    return result;
  }
}
