/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.bcmodel.role;

import max.model.ledger.blockchain.role.RBlockCreator;
import max.model.ledger.blockchain.role.RBlockchainMaintainer;

/**
 * Role representing capabilities of our blockchain miners:
 *
 * <ul>
 *   <li>Creating blocks
 *   <li>Maintaining the blockchain
 * </ul>
 */
public interface RBCMiner extends RBlockCreator, RBlockchainMaintainer {}
