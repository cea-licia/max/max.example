open module max.example.bcmodel {
    requires max.core;
    requires max.datatype.ledger;
    requires max.datatype.com;
    requires max.model.network.p2p;
    requires max.model.ledger.blockchain;
    requires java.logging;
}