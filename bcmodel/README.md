# max.example.bcmodel

This project is pas of the set of examples the MAX team is providing to the community.

The Bitcoin-like model tutorial is available on the MAX website and will teach you step by step how
to create a blockchain model based on the Bitcoin blockchain.

This repository contains all the code you will write during this tutorial.

## Model description

### BCAgent

Represents a node in our blockchain. It can be either a user (which issues transactions) or a
miner (which creates blocks).

### BCEnvironment

A simple `BlockchainEnvironment` that allows custom roles and creates `BCContext` instances.

### Roles

The model defines two different roles:

* `RBCMiner`, which extends `RBlockCreator` and `RBlockchainMaintainer`. Played when the agent is a
  miner.
* `RBCUser`, which is an alias for `RBlockchainUser`. Played when the agent issues transactions.

## Messages

Agents are communicating using a dedicated set of messages:

* `TX` messages, which contain issues transactions to be distributed across the network
* `BLOCK` messages, which contain a block
* `GET_BLOCK` messages, which contain the hash of a requested block. Mostly used when an agent
  receives a block but isn't aware of its parent: in that case, it asks its neighbors about that
  block.

## Actions

This model defines two actions:
1. `ACCreateBCBlock`, which is called each time the agent must create a new block.
2. `ACIssueTransaction`, which is called by blockchain users to issue a new transaction

More details about each action in their class definition.
