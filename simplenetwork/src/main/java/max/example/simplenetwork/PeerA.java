/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.simplenetwork;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import max.core.EnvironmentEvent;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.action.ReactiveAction;
import max.core.scheduling.ActionActivator;
import max.model.network.p2p.action.ACDiscoverNeighbors;
import max.model.network.p2p.action.PeerAgent;
import max.model.network.p2p.env.message.MessageEvent;
import max.model.network.p2p.role.RNetworkPeer;

public class PeerA extends PeerAgent {
	
	public PeerA(String envName) {
		this(new Plan<PeerAgent>() {
			@Override
			public List<ActionActivator<PeerAgent>> getInitialPlan() {
				return Arrays.asList(
						// at initilization, take the role RNetworkPeer
						new ACTakeRole<PeerAgent>(envName, RNetworkPeer.class, null).oneTime(0),
						// right after, discover neighbours in the environment
						new ACDiscoverNeighbors<>(envName, null).oneTime(1),
						// at tick 10, send an order message to B
						new ACSendOrderMessage<PeerAgent>(envName, RNetworkPeer.class, null).oneTime(10));
			}

			@Override
			public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<PeerAgent>>> getEventBoundActions() {
				return Map.of(MessageEvent.class,
						Collections.singletonList(new ACHandleSimpleNetworkMessages<>(envName, null)));
			}
		});
	}

	public PeerA(Plan<? extends PeerAgent> plan) {
		super(plan);
	}

}
