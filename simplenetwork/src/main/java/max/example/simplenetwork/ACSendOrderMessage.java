/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.simplenetwork;

import java.math.BigDecimal;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.datatype.com.Message;
import max.model.network.p2p.action.PeerAgent;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.role.RNetworkPeer;

/**
 *
 * @author Önder Gürcan
 *
 * @param <A> Agent's type
 */
public class ACSendOrderMessage<A extends PeerAgent> extends Action<A> {

	public ACSendOrderMessage(String environment, Class<? extends Role> role, A owner, Object ... inputs) {
		super(environment, role, owner, inputs);
	}

	@Override
	public void execute() {
		final P2PContext context = (P2PContext) getOwner().getContext(getEnvironment());
		
		// get the sender's address
		final AgentAddress ownerAddress = context.getMyAddress(RNetworkPeer.class.getName());
		
		// get the neighbor's address
		final AgentAddress neighborAddress = context.getConnectionsList().getNeighborsAddresses().get(0);
		
		// create the order message with a string payload
		String payload = "Your objective is X";
		final Message<AgentAddress, String> orderMessage = new Message<AgentAddress, String>(ownerAddress, neighborAddress, payload);
		orderMessage.setType("ORDER");
		
		// get the environment where the agents are located
		P2PEnvironment p2pEnvironment = (P2PEnvironment) context.getEnvironment();
		
		// send the message through the environment
		BigDecimal currentTick = getOwner().getCurrentTick();
		System.out.println("[" + currentTick + "][A] I sent an ORDER message to B: " + payload);
		p2pEnvironment.sendMessage(orderMessage);		
	}

	@Override
	public <T extends Action<A>> T copy() {
		return (T) new ACSendOrderMessage<>(getEnvironment(), getRole(), getOwner());
	}
}
