/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.simplenetwork;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.action.ReactiveAction;
import max.core.role.Role;
import max.datatype.com.Message;
import max.model.network.p2p.action.PeerAgent;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.ACCEPTMessageHandler;
import max.model.network.p2p.env.message.CONNECTMessageHandler;
import max.model.network.p2p.env.message.MessageHandler;
import max.model.network.p2p.env.message.REJECTMessageHandler;
import max.model.network.p2p.role.RNetworkPeer;

import java.util.HashMap;
import java.util.Map;

/**
 * Action handling messages in a given environment. For know, check all agent mailboxes.
 * By default, this action can handle all messages defined in {@link max.model.network.p2p.env.P2PProtocolFactory}. You
 * can provide custom message handlers if you want to extend the supported messages list.
 *
 * @see MessageHandler
 * @author Önder Gürcan
 * @param <A> Action's owner type
 * @max.role {@link RNetworkPeer}
 */
public class ACHandleSimpleNetworkMessages<A extends PeerAgent> extends ReactiveAction<A> {

    /** Registered handlers. */
    private final Map<String, MessageHandler> handlers;

    /**
     * Default constructor.
     *
     * @param environment In which environment action will handle incoming messages
     * @param owner Action's owner
     */
    public ACHandleSimpleNetworkMessages(final String environment, final A owner) {
        super(environment, RNetworkPeer.class, owner);
        handlers = new HashMap<>();

        registerMessageType(CONNECTMessageHandler.MESSAGE_TYPE, new CONNECTMessageHandler());
        registerMessageType(ACCEPTMessageHandler.MESSAGE_TYPE, new ACCEPTMessageHandler());
        registerMessageType(REJECTMessageHandler.MESSAGE_TYPE, new REJECTMessageHandler());
        registerMessageType("TEMP", new TEMPMessageHandler());
    }

    @Override
    public void execute() {
        final P2PContext context = (P2PContext) getOwner().getContext(getEnvironment());
        if (context == null) {
            return;  // Message received in another environment.
        }
        final P2PEnvironment env = (P2PEnvironment) context.getEnvironment();

        for (final Class<? extends Role> role : getOwner().getMyRoles(getEnvironment())) {
            final AgentAddress ownerAddress = context.getMyAddress(role.getName());
            if (ownerAddress != null) {
                Message<AgentAddress, ?> message = null;
                do {
                    message = env.pollMessage(ownerAddress);
                    if (message != null) {
                        final MessageHandler handler = handlers.get(message.getType());
                        if (handler != null) {
                            handler.handle(context, message);
                        }
                    }

                } while (message != null);
            }
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        var copy = new ACHandleSimpleNetworkMessages<>(getEnvironment(), getOwner());
        copy.handlers.putAll(handlers);
        return (T) copy;
    }

    /**
     * Add a new {@link MessageHandler}. Replace old association, meaning you can override default handlers with your
     * own implementation.
     *
     * @param messageType The message type this handler will be associated to
     * @param handler Handler to add
     */
    public void registerMessageType(final String messageType, final MessageHandler handler) {
        handlers.put(messageType, handler);
    }
}
