/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.simplenetwork;

import java.math.BigDecimal;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.datatype.com.Message;
import max.model.network.p2p.action.PeerAgent;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * Role: P2PEnvironment
 * 
 * @author Önder Gürcan
 *
 * @param <A> Agent's type
 */
public class ACSendTempMessage<A extends PeerAgent> extends Action<A> {

	public ACSendTempMessage(String environment, Class<? extends Role> role, A owner, Object ... inputs) {
		super(environment, role, owner, inputs);
	}

	@Override
	public void execute() {
		final P2PContext context = (P2PContext) getOwner().getContext(getEnvironment());
		
		// get the sender's address
		final AgentAddress ownerAddress = context.getMyAddress(RNetworkPeer.class.getName());
		
		// get the neighbor's address
		final AgentAddress neighborAddress = context.getConnectionsList().getNeighborsAddresses().get(0);
		
		// create the temp message with a string payload
		int temp = (int)(Math.random() * 10) + 30;
		String payload = "The temperature is " + temp;
		final Message<AgentAddress, String> tempMessage = new Message<AgentAddress, String>(ownerAddress, neighborAddress, payload);
		tempMessage.setType("TEMP");
		
		// get the environment where the agents are located
		P2PEnvironment p2pEnvironment = (P2PEnvironment) context.getEnvironment();
		
		// send the message through the environment
		BigDecimal currentTick = getOwner().getCurrentTick();
		System.out.println("[" + currentTick + "][B] I sent a TEMP message to A: " + payload);
		p2pEnvironment.sendMessage(tempMessage);
	}

	@Override
	public <T extends Action<A>> T copy() {
		return (T) new ACSendTempMessage<>(getEnvironment(), getRole(), getOwner());
	}
}
