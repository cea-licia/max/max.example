/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.simplenetwork;

import java.math.BigDecimal;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.p2p.action.ACHandleMessages;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.message.MessageHandler;

/**
 * Special handler designed to handle ACCEPT messages.
 *
 * @see max.model.network.p2p.action.ACHandleMessages
 * @see max.model.network.p2p.env.message.MessageHandler
 * @author Önder Gürcan
 */
public class TEMPMessageHandler implements MessageHandler {

    /** Message type handled by this handler. */
    public static final String MESSAGE_TYPE = "TEMP";

    @Override
    public void handle(P2PContext context, Message<AgentAddress, ?> message) {
    	Object payload = message.getPayload();
    	BigDecimal currentTick = context.getOwner().getCurrentTick();
    	System.out.println("[" + currentTick + "][A] I received the message from B: " + payload);
    }
}
