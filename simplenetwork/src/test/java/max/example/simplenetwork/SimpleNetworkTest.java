/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.simplenetwork;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

import max.core.EnvironmentEvent;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.action.ReactiveAction;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.model.network.p2p.action.ACDiscoverNeighbors;
import max.model.network.p2p.action.PeerAgent;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageEvent;
import max.model.network.p2p.role.RNetworkPeer;

/**
 * 
 * 
 * @author Önder Gürcan
 *
 */
public class SimpleNetworkTest {

	@BeforeEach
	public void before(@TempDir Path tempDir) {
		// clear all parameter values
		clearParameters();
		// set simulation step to 1
		setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
		// set UI mode to Silent
		setParameter("MAX_CORE_UI_MODE", "Silent");
		// set the maximum outbound connections to the maximum integer value
		setParameter("MAX_CORE_MAX_OUTBOUND_CONNECTIONS", Integer.toString(Integer.MAX_VALUE));
		// set the maximum inbound connections to the maximum integer value
		setParameter("MAX_CORE_MAX_INBOUND_CONNECTIONS", Integer.toString(Integer.MAX_VALUE));
		// set the network delay to zero
		setParameter("MAX_MODEL_NETWORK_DELAY", BigDecimal.ZERO.toString());
		// set the netwrok reliability to one
		setParameter("MAX_MODEL_NETWORK_RELIABILITY", "1");
		// set the neighbour discovery frequency to one
		setParameter("MAX_MODEL_NETWORK_NEIGHBOUR_DISCOVERY_FREQUENCY", "1");
		setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
	}

	/**
	 * Test if two NetworkAgent send messages to each other as neighbors.
	 */
	@Test
	public void simpleMessaging(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent experimenterAgent = new ExperimenterAgent() {
			private P2PEnvironment p2pEnvironment;
			private PeerAgent peerAgent1;
			private PeerAgent peerAgent2;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Create and launch the environment
				p2pEnvironment = new P2PEnvironment();
				agents.add(p2pEnvironment);
				String envName = p2pEnvironment.getName();

				// Create and launch the agents
				peerAgent1 = new PeerA(envName);
				agents.add(peerAgent1);

				peerAgent2 = new PeerB(envName);
				agents.add(peerAgent2);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// do not test anything for the moment
			}
		};

		launchTester(experimenterAgent, 100, testInfo);
	}
}
