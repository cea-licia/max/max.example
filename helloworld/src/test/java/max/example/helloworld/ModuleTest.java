/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.helloworld;

import static max.core.test.TestMain.launchTester;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import max.core.MAXParameters;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.core.role.RActor;
import max.core.scheduling.ActionActivator;
import max.example.helloworld.action.ACHelloWorld;
import max.example.helloworld.exp.HWExperimenter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * The second way to execute your model is to define a bunch of tests.
 *
 * <p>To do so:
 *
 * <ol>
 *   <li>Create a test class
 *   <li>Write a test method:
 *       <ol>
 *         <li>Create and setup an experimenter like in {@link
 *             max.example.helloworld.exp.HWExperimenter}
 *         <li>Call method {@link max.core.test.TestMain#launchTester(ExperimenterAgent, int,
 *             TestInfo)}
 *       </ol>
 *   <li>Execute the test using your IDE or command
 *       <pre>mvn clean test</pre>
 * </ol>
 *
 * @author Pierre Dubaillay
 */
public class ModuleTest {

  /** Set some required parameters for all tests. */
  @BeforeAll
  public static void beforeAll(@TempDir final Path resultDir) {
    MAXParameters.setParameter("MAX_CORE_SIMULATION_STEP", "1");
    MAXParameters.setParameter("MAX_CORE_UI_MODE", "SILENT");
    MAXParameters.setParameter("MAX_CORE_RESULTS_FOLDER_NAME", resultDir.toString());
  }

  /**
   * This test will use the experimenter we defined and called {@link
   * max.example.helloworld.exp.HWExperimenter}.
   *
   * <p>The experiment duration is set to 100 ticks.
   *
   * @param testInfo Injected by JUnit5
   * @throws Throwable Any error occurring during the simulation
   */
  @Test
  public void testUsingAnExistingExperimenter(final TestInfo testInfo) throws Throwable {
    launchTester(new HWExperimenter(), 100, testInfo);
  }

  /**
   * This time we define an anonymous experimenter and setup its scenario.
   *
   * <p>Contrary to {@link HWExperimenter} scenario, the environment will be named 'HW
   * Environment-Test' and the agent 'Alice'. Otherwise, the experiment is the same.
   *
   * @param testInfo Injected by JUnit5
   * @throws Throwable Any error occurring during the simulation
   */
  @Test
  public void testUsingAnAnonymousExperimenter(final TestInfo testInfo) throws Throwable {
    final var experimenter =
        new ExperimenterAgent() {

          @Override
          protected List<MAXAgent> setupScenario() {
            // First we create our environment
            final var environment = new SimulatedEnvironment();
            environment.setName("HW Environment-Test");

            /*
             * Then we create the agent's plan.
             * The plan will contain two actions: one to join our custom environment and our ACHelloWorld action.
             */
            var plan =
                new Plan<>() {
                  @Override
                  public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
                    return List.of(
                        new ACTakeRole<SimulatedAgent>(environment.getName(), RActor.class, null)
                            .oneTime(0),
                        new ACHelloWorld(environment.getName(), null).oneTime(5));
                  }
                };

            // We can now create our agent
            var agent = new SimulatedAgent(plan);
            agent.setName("Alice");

            // We must return all created agents, including the environment
            return List.of(environment, agent);
          }
        };
    launchTester(experimenter, 100, testInfo);
  }

  /**
   * We can even mix both: create an anonymous experimenter extending an existing experimenter.
   *
   * <p>Here we are modifying the {@link HWExperimenter} scenario by adding a second agent named
   * 'Alice'. Alice will say hello at tick 10.
   *
   * @param testInfo Injected by JUnit5
   * @throws Throwable Any error occurring during the simulation
   */
  @Test
  public void testWithAnAnonymousExperimenterExtendingHWExperimenter(final TestInfo testInfo)
      throws Throwable {
    final var experimenter =
        new HWExperimenter() {

          @Override
          protected List<MAXAgent> setupScenario() {
            // Get scenario defined in super class
            final List<MAXAgent> agents = new ArrayList<>(super.setupScenario());

            // Add Alice agent saying hello at tick 10
            // First, the plan
            final var plan =
                new Plan<>() {

                  @Override
                  public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
                    return List.of(
                        new ACTakeRole<SimulatedAgent>(
                                agents.get(0).getName(), // Little hack to get environment
                                RActor.class,
                                null)
                            .oneTime(0),
                        new ACHelloWorld(
                                agents.get(0).getName(), // Little hack to get environment
                                null)
                            .oneTime(10));
                  }
                };

            // Then Alice
            final var alice = new SimulatedAgent(plan);
            alice.setName("Alice");
            agents.add(alice);

            return agents;
          }
        };
    launchTester(experimenter, 100, testInfo);
  }
}
