/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.helloworld.action;

import max.core.agent.SimulatedAgent;
import max.core.action.Action;
import max.core.role.RActor;

import java.util.logging.Level;

/**
 * This is our first custom {@link Action}.
 *
 * <p>It will use the owner's logger to print a nice "Hello World" message. The action requires the
 * role {@link RActor}.
 *
 * @author Pierre Dubaillay
 * @max.role {@link RActor}
 */
public class ACHelloWorld extends Action<SimulatedAgent> {

  /**
   * Default constructor building a new {@link ACHelloWorld} instance.
   *
   * @param environment where the action will run
   * @param owner by whom
   */
  public ACHelloWorld(final String environment, final SimulatedAgent owner) {
    super(environment, RActor.class, owner);
  }

  @Override
  public void execute() {
    getOwner()
        .getLogger()
        .log(
            Level.INFO,
            String.format(
                "[%.1f] Agent '%s' says \"Hello World !\"",
                getOwner().getSimulationTime().getCurrentTick().doubleValue(),
                getOwner().getName()));
  }

  /**
   * {@inheritDoc} Since the action is used in a plan, we must override the {@link Action#copy()}
   * and return a copy of the current instance.
   */
  @Override
  public <T extends Action<SimulatedAgent>> T copy() {
    return (T) new ACHelloWorld(getEnvironment(), getOwner());
  }
}
