/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.helloworld.inspection.collector;

import java.util.List;
import java.util.Map;
import max.core.SimulationProbe;
import max.core.agent.SimulatedAgent;
import max.core.inspection.DataInspector;
import max.core.inspection.collector.EachTickCollector;
import max.core.role.RActor;

/**
 * Collector collecting the number of agents each tick.
 *
 * <p>Based on the fact that all {@link SimulatedAgent} are taking the role {@link RActor}. Thus,
 * only {@link SimulatedAgent} are counted, not environment(s), experimenter, scheduler or
 * inspector(s).
 */
public class AgentNumberCollector extends EachTickCollector<List<?>> {

  // The probe used to watch agents taking the {@link RActor} role. */
  private final SimulationProbe<SimulatedAgent> agentProbe;

  /**
   * Default constructor with default metadata.
   *
   * @param owner inspector that owns the collector
   * @param environment where data is collected
   */
  public AgentNumberCollector(DataInspector owner, String environment) {
    super(
        owner,
        Map.of(
            "title", "Agent count over time",
            "chart-type", "BAR_CHART",
            "header", List.of("tick", "nb of agents")));
    agentProbe = new SimulationProbe<>(environment, RActor.class);
  }

  @Override
  public void activate() {
    getOwner().addProbe(agentProbe);
    super.activate();
  }

  /** Collects how many agents are in the environment. */
  @Override
  protected void collectData() {
    final var agents = agentProbe.getCurrentAgentsList();
    export(List.of(getCurrentTime(), agents.size()));
  }
}
