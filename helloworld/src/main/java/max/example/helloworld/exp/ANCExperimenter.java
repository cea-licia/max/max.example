/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.helloworld.exp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import max.core.MAXParameters;
import max.core.action.ACLeaveEnvironment;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.core.inspection.DataInspector;
import max.core.inspection.exporter.CSVFileExporter;
import max.core.role.RActor;
import max.example.helloworld.inspection.collector.AgentNumberCollector;

/**
 * The ANCExperimenter custom experimenter.
 *
 * <p>This experimenter is designed to test the {@link AgentNumberCollector}. It will create agents
 * with a fixed lifetime and coming to life at different ticks in the simulation.
 *
 * <p>You must define the "MAX_EXAMPLE_HELLOWORLD_NB_OF_AGENTS" parameter in order to this
 * experimenter to work.
 *
 * <p>The application can be launched with the following command:
 *
 * <pre>
 * java -jar HelloWorldApp-jar-with-dependencies.jar run max/example/helloworld/anc.xml
 * </pre>
 *
 * @author Pierre Dubaillay
 */
public class ANCExperimenter extends ExperimenterAgent {

  // Store the environment
  private SimulatedEnvironment environment;

  @Override
  protected List<MAXAgent> setupScenario() {
    // Store created agents
    final List<MAXAgent> agents = new ArrayList<>();

    // First we create our environment
    environment = new SimulatedEnvironment();
    environment.setName("ANC Environment");
    agents.add(environment);

    // Get some parameters values
    final int nbOfAgents = MAXParameters.getIntegerParameter("MAX_EXAMPLE_HELLOWORLD_NB_OF_AGENTS");
    final int simulationDuration = MAXParameters.getIntegerParameter("MAX_CORE_TIMEOUT_TICK");

    for (var i = 0; i < nbOfAgents; ++i) {
      // Compute start and end tick.
      final var startTick = BigDecimal.valueOf(Math.ceil(Math.random() * simulationDuration / 2));
      final var endTick =
          startTick.add(BigDecimal.valueOf(Math.ceil(Math.random() * simulationDuration / 2)));

      // We can now create our agent
      final var agent = new SimulatedAgent(buildPlan(startTick, endTick));
      agent.setName(String.format("Bob[%d]", i));
      agents.add(agent);
    }

    // We must return all created agents, including the environment
    return agents;
  }

  @Override
  protected List<DataInspector> setupDataInspectors() {
    final var inspector = new DataInspector();

    if ("ON"
        .equalsIgnoreCase(
            MAXParameters.getParameter("MAX_EXAMPLE_HELLOWORLD_AGENT_NUMBER_COLLECTOR"))) {
      final var collector = new AgentNumberCollector(inspector, environment.getName());
      collector.addExporter(
          new CSVFileExporter(getOutputFolder().resolve("anc.csv"), collector.getMetadata()));
    }

    return List.of(inspector);
  }

  /**
   * Build a very simple plan: the agent takes {@link RActor} role at tick {@code startTick} and
   * leaves the environment at tick {@code endTick}.
   *
   * @param startTick when the agent enters the environment
   * @param endTick when the agent leaves the environment
   * @return the plan
   */
  private Plan<SimulatedAgent> buildPlan(BigDecimal startTick, BigDecimal endTick) {
    return () ->
        List.of(
            new ACTakeRole<SimulatedAgent>(environment.getName(), RActor.class, null)
                .oneTime(startTick),
            new ACLeaveEnvironment<SimulatedAgent>(environment.getName(), null).oneTime(endTick));
  }
}
