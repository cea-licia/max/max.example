/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.example.helloworld.exp;

import java.util.List;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.core.role.RActor;
import max.core.scheduling.ActionActivator;
import max.example.helloworld.action.ACHelloWorld;

/**
 * The HelloWorld custom experimenter.
 *
 * <p>This is the first way of running an experiment:
 *
 * <ol>
 *   <li>Create a custom {@link ExperimenterAgent}
 *   <li>Override the {@link ExperimenterAgent#setupScenario()} method to create a scenario
 *   <li>Create a configuration file and set property "MAX_CORE_EXPERIMENTER_NAME" to the
 *       experimenter class name
 *   <li>package the module
 *   <li>Launch the created jar with 'java -jar XXX.jar run path/to/config/file.xml
 * </ol>
 *
 * Here we are going to create a {@link max.core.agent.SimulatedAgent} with a simple {@link
 * max.core.action.Plan}. Our agent will join a created {@link max.core.agent.SimulatedEnvironment}
 * and execute an action in it.
 *
 * <p>The application can be launched with the following command:
 *
 * <pre>java -jar HelloWorldApp-jar-with-dependencies.jar run max/example/helloworld/default.xml
 * </pre>
 *
 * @author Pierre Dubaillay
 */
public class HWExperimenter extends ExperimenterAgent {

  private SimulatedEnvironment environment;

  @Override
  protected List<MAXAgent> setupScenario() {
    // First we create our environment
    environment = new SimulatedEnvironment();
    environment.setName("HW Environment");

    /*
     * Then we create the agent's plan.
     * The plan will contain two actions: one to join our custom environment and our ACHelloWorld action.
     */
    var plan =
        new Plan<>() {
          @Override
          public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
            return List.of(
                new ACTakeRole<SimulatedAgent>(environment.getName(), RActor.class, null).oneTime(0),
                new ACHelloWorld(environment.getName(), null).oneTime(5));
          }
        };

    // We can now create our agent
    var agent = new SimulatedAgent(plan);
    agent.setName("Bob");

    // We must return all created agents, including the environment
    return List.of(environment, agent);
  }
}
