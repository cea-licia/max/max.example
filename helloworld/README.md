# max.example.helloworld

This project is part of the set of examples the MAX team is providing to the community.

The Hello World tutorial is available on the MAX website and will teach you step by step how to:
1. Create an empty project
2. Create your first action
3. Create your first Experimenter

This repository contains all the code you will write during the tutorial.

## Model description

This basic model contains:
1. one action named `ACHelloWorld` which prints a nice message using its owner's logger
2. one experimenter named `HWExperimenter` which setups a simple simulation
3. one test class named `ModuleTest` to explore how to use JUnit5 to run a simulation
