# max.example

This repository contains a set of examples covering most aspects of MAX:

- helloworld: contains a basic example of how to create a simple experiment
- simplenetwork: lear how to play with messages
- cellular-life: a more complex model with some GUI
- bcmodel: a full example of how to create a customized blockchain model

## About MAX

__MAX__ (for 'Multi-Agent eXperimenter') is a framework for agent-based simulation and full
agent-based applications building. The objective of __MAX__  is to make rapid prototyping of
industrial cases and to carry out their feasibility analysis in a realistic manner.

__MAX__ is particularly well suited for modeling open, distributed and intelligent systems
developing over time. It embeds the Agent/Environment/Role (AER) organizational model, includes
tools and components useful for developing and testing simulation models and is written in Java 11
to run on any device.

You want to know if MAX is suited for your project ? Check our [website]() to find it out!