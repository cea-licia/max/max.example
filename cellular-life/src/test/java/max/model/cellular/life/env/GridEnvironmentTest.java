/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.env;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import max.core.ExperimenterAction;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationSchedule;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.cellular.life.action.ACCalculateNumberOfLiveNeighbors;
import max.model.cellular.life.action.ACRequestNeighbors;
import max.model.cellular.life.action.ACUpdateState;
import max.model.cellular.life.action.Cell;
import max.model.cellular.life.action.CellState;
import max.model.cellular.life.action.IObservedCell;
import max.model.cellular.life.role.RCell;
import org.junit.jupiter.api.io.TempDir;

/**
 * We fix the test parameters: The step simulation is fixed to ONE The mode is
 * fixed to Silent
 */
public class GridEnvironmentTest {

	@BeforeEach
	public void before(@TempDir Path tempDir) {
		// clear all parameter values
		clearParameters();
		// set simulation step to 1
		setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
		// set UI mode to Silent
		setParameter("MAX_CORE_UI_MODE", "Silent");
		// set the initial states of cell to not random, i.e. state=DEAD
		setParameter("MAX_MODEL_CELLULAR_LIFE_RANDOM_STATE", "false");
		// set the initial density of live cells
		setParameter("MAX_MODEL_CELLULAR_LIFE_INITIAL_DENSITY", "0.35");
		setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
	}

	@Test
	public void allowsRoles() {
		GridEnvironment grid = new GridEnvironment(2, 2);
		assertTrue(grid.allowsRole(RCell.class));
	}

	/**
	 * GridEnvironment is by default empty and it does not create Cells by time. It
	 * is not its responsibility.
	 */
	@Test
	public void createEmptyEnvironment(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 2;
			int numOfColumns = 2;
			GridEnvironment gridEnvironment = null;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing if the
				// GridEnvironment is always empty.
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.ONE,
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertTrue(gridEnvironment.isEmpty());
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a given size (e.g., 5x5). Then, enough
	 * number of Cell agents (i.e. 5x5=25) should be created with an initial
	 * ACTakeRole action to enter into the pre-created GridEnvironment. As a result,
	 * all Cell agents should enter into GridEnvironment with an initial state of
	 * Dead and with a unique position in the 2D grid.
	 */
	@Test
	public void createAnEnvironmentAndFillIt(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 5;
			int numOfColumns = 5;

			GridEnvironment gridEnvironment = null;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan with RCell Role
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
								new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}

				return agents;

			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing if the
				// GridEnvironment is always full.
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.ONE,
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// check that the environment is not empty
						assertFalse(gridEnvironment.isEmpty());
						// check the expected size
						assertTrue(gridEnvironment.getNumberOfCells() == numOfRows * numOfColumns);
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size of 1x2 which is not supported. An
	 * exception must be thrown.
	 * 
	 */
	@Test
	public void createAnIllegalGrid12(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 1;
			int numOfColumns = 2;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				assertThrows(IllegalArgumentException.class, () -> {
					gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				});
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.ONE,
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {

					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size of 2x2. Four agents should be created
	 * with an initial ACTakeRole action to enter into the pre-created
	 * GridEnvironment and ACRequestneighbors action to set the list of the
	 * neighbors in the environment. Each cell has either 3 neighbors.
	 * 
	 */
	@Test
	public void checkNumberOfNeighborsInTheGrid22(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 2;
			int numOfColumns = 2;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),

								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing the number of neighbors
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(1),
						BigDecimal.ONE);
				// Verify that each cell has 3 alive neighbors
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();
						// get the contexts
						for (int i = 0; i < numOfRows; i++) {
							for (int j = 0; j < numOfColumns; j++) {
								Cell cell = cells[i][j];
								GridContext context_ij = (GridContext) cell
										.getContext(Organization.GRID_ENVIRONMENT.toString());
								int numberOfneighbors = context_ij.getNeighbors().size();
								assertTrue(numberOfneighbors == 3);
							}
						}
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a given size (e.g., 3x3). A number of Cell
	 * agents (i.e. 3x3=9) should be created with an initial ACTakeRole action to
	 * enter into the pre-created GridEnvironment and ACRequestneighbors action to
	 * set the list of the neighbors in the environment. Each cell has either 3, 5
	 * or 8 neighbors depending on its position (corner, border or central cell
	 * respectivly)
	 * 
	 */
	@Test
	public void checkNumberOfNeighborsInTheGrid33(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 3;
			int numOfColumns = 3;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();
				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),

								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an repeating action for testing the number of live neighbors

				// start at tick 2
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(1),
						BigDecimal.ONE);
				// Verify that each cell has either 3, 5 or 8 neighbors
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						// get the contexts
						for (int i = 0; i < numOfRows; i++) {
							for (int j = 0; j < numOfColumns; j++) {
								Cell cell = cells[i][j];
								GridContext context_ij = (GridContext) cell
										.getContext(Organization.GRID_ENVIRONMENT.toString());
								// get neighbors
								int numberOfneighbors = context_ij.getNeighbors().size();
								assertTrue(numberOfneighbors == 3 || numberOfneighbors == 5 || numberOfneighbors == 8);
							}
						}
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);

	}

	/**
	 * We create a GridEnvironment with a given size (e.g., 6x3). A number of Cell
	 * agents (i.e. 6x3=18) should be created with an initial ACTakeRole action to
	 * enter into the pre-created GridEnvironment and ACRequestneighbors action to
	 * set the list of the neighbors in the environment. This test verifies the
	 * right position of the neighbors.
	 * 
	 */
	@Test
	public void checkTheRightPositionOfNeighborsInTheGrid63(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 6;
			int numOfColumns = 3;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),
								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < numOfRows; i++) {
					for (int j = 0; j < numOfColumns; j++) {
						Cell cell = new Cell(rawPlan);
						agents.add(cell);
					}
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing if the
				// neighbors are in the right positions

				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(1),
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						// get the contexts
						for (int i = 0; i < numOfRows; i++) {
							for (int j = 0; j < numOfColumns; j++) {
								Cell cell = cells[j][i];
								GridContext context_ij = (GridContext) cell
										.getContext(Organization.GRID_ENVIRONMENT.toString());
								List<IObservedCell> neighbors = context_ij.getNeighbors();

								int x = context_ij.getPosX();
								int y = context_ij.getPosY();

								for (int k = 0; k < neighbors.size(); k++) { // check the cell neighbors positions
									Cell cell_k = (Cell) neighbors.get(k);
									GridContext context_k = (GridContext) cell_k
											.getContext(Organization.GRID_ENVIRONMENT.toString());
									int neighbor_x = context_k.getPosX();
									int neighbor_y = context_k.getPosY();
									assertTrue(x - 1 <= neighbor_x && neighbor_x <= x + 1);
									assertTrue(y - 1 <= neighbor_y && neighbor_y <= y + 1);
								}
							}
						}
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size 3x3. A number of Cell agents should
	 * be created with an initial ACTakeRole action to enter into the pre-created
	 * GridEnvironment and ACRequestneighbors action to set the list of the
	 * neighbors in the environment. We set some of the cells 0,0 and 2,2 in LIVE
	 * state. Then we must verify that the number of live neighbors is either 0, 1
	 * or 2 in our grid
	 * 
	 */
	@Test
	public void checkNumberOfLiveNeighbours(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 3;
			int numOfColumns = 3;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),
								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < numOfRows * numOfColumns; i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing if the
				// GridEnvironment is always full.

				ActivationSchedule schedule0 = ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(1));
				schedule(new ActionActivator<>(schedule0, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						// all neighbours are made alive
						Cell cell = cells[0][0];
						GridContext context_ij = (GridContext) cell
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[2][2];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);
					}
				}));

				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
						BigDecimal.ONE);

				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						// get the contexts
						for (int i = 0; i < numOfRows; i++) {
							for (int j = 0; j < numOfColumns; j++) {
								Cell cell = cells[i][j];
								GridContext context_ij = (GridContext) cell
										.getContext(Organization.GRID_ENVIRONMENT.toString());
								// get live neighbours
								int liveNeighbours = context_ij.getNumberOfLiveNeighbors();
								assertTrue(liveNeighbours == 0 || liveNeighbours == 1 || liveNeighbours == 2);
							}
						}
					}
				}));
			}
		};
		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size 4x6. A number of Cell agents should
	 * be created with an initial ACTakeRole action to enter into the pre-created
	 * GridEnvironment and ACRequestneighbors action to set the list of the
	 * neighbors in the environment. We set some of the cell 3,4 in LIVE state. Then
	 * we must verify that the state of each cell is updated.
	 * 
	 */
	@Test
	public void updateStatesToDead(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 4;
			int numOfColumns = 6;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				// The agents have three roles: RCell, ACRequestLiveneighbors and ACUpdateState
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),

								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)),

								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
												BigDecimal.valueOf(2)),
										new ACCalculateNumberOfLiveNeighbors<>(Organization.GRID_ENVIRONMENT.toString(),
												RCell.class, null)),

								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3),
												BigDecimal.valueOf(2)),
										new ACUpdateState<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action one at tick 1 to set some cells alive
				ActivationSchedule schedule0 = ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(1));
				schedule(new ActionActivator<>(schedule0, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						Cell[][] cells = gridEnvironment.getGrid();
						// set cell 3x4 to alive state
						Cell cell = cells[4][3];
						GridContext context_ij = (GridContext) cell
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);
					}

				}));

				// Schedule a repeating action to verify the number of live neighbors after the
				// update
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(4),
						BigDecimal.valueOf(2));

				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						// get the contexts
						for (int i = 0; i < numOfColumns; i++) {
							for (int j = 0; j < numOfRows; j++) {
								Cell cell = cells[i][j];
								GridContext context_ij = (GridContext) cell
										.getContext(Organization.GRID_ENVIRONMENT.toString());
								// get live neighbors
								int liveneighbors = context_ij.getNumberOfLiveNeighbors();
								assertTrue(liveneighbors == 0);
							}
						}
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size 2x2. A number of Cell agents should
	 * be created with an initial ACTakeRole action to enter into the pre-created
	 * GridEnvironment and ACRequestneighbors action to set the list of the
	 * neighbors in the environment. We set three of the cells in LIVE state. Then
	 * we must verify that the fourth cell is alive.
	 * 
	 */
	@Test
	public void updateStateToAlive(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 2;
			int numOfColumns = 2;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),

								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)),
								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
												BigDecimal.valueOf(2)),
										new ACCalculateNumberOfLiveNeighbors<>(Organization.GRID_ENVIRONMENT.toString(),
												RCell.class, null)),

								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3),
												BigDecimal.valueOf(2)),
										new ACUpdateState<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action once at tick 1 to set the initial cells states.
				ActivationSchedule schedule0 = ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(1));

				schedule(new ActionActivator<>(schedule0, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// set cell 3x4 to alive state
						Cell[][] cells = gridEnvironment.getGrid();

						// some cells are made alive
						Cell cell = cells[0][0];
						GridContext context_ij = (GridContext) cell
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[0][1];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[1][1];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);
					}

				}));

				// schedule a repeating action starting from tick 3 to check the number of live
				// neighbors
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(4),
						BigDecimal.valueOf(2));

				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						// get the contexts
						for (int i = 0; i < numOfColumns; i++) {
							for (int j = 0; j < numOfRows; j++) {
								Cell cell = cells[i][j];
								GridContext context_ij = (GridContext) cell
										.getContext(Organization.GRID_ENVIRONMENT.toString());
								// get live neighbors
								int liveneighbors = context_ij.getNumberOfLiveNeighbors();
								assertTrue(liveneighbors == 3);
							}
						}
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size 3x4. A number of Cell agents should
	 * be created with an initial ACTakeRole action to enter into the pre-created
	 * GridEnvironment and ACRequestneighbors action to set the list of the
	 * neighbors in the environment. We set just two neighbor cells in LIVE state.
	 * Then we must verify the cells in the neighbor remain in their initial state.
	 * 
	 */

	@Test
	public void cellRemainInItsState(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 3;
			int numOfColumns = 4;
			private GridEnvironment gridEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),

								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)),

								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
												BigDecimal.valueOf(2)),
										new ACCalculateNumberOfLiveNeighbors<>(Organization.GRID_ENVIRONMENT.toString(),
												RCell.class, null)),

								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3),
												BigDecimal.valueOf(2)),
										new ACUpdateState<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing if the
				// GridEnvironment is always full.

				ActivationSchedule schedule0 = ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(1));

				schedule(new ActionActivator<>(schedule0, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// set cell 1x1 and 2x2 to alive state
						Cell[][] cells = gridEnvironment.getGrid();

						// all neighbors are made alive
						Cell cell = cells[1][1];
						GridContext context_ij = (GridContext) cell
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[2][2];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						System.out.println(gridEnvironment.displayAsString());
					}

				}));

				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
						BigDecimal.valueOf(2));
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// get the environment cells
						Cell[][] cells = gridEnvironment.getGrid();

						Cell cell = cells[1][2];
						GridContext context_ij = (GridContext) cell
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						assertTrue(context_ij.getCellState() == CellState.DEAD);

						cell = cells[2][1];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						assertTrue(context_ij.getCellState() == CellState.DEAD);

						cell = cells[1][1];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[2][2];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						System.out.println(gridEnvironment.displayAsString());
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

	/**
	 * We create a GridEnvironment with a size 3x4. A number of Cell agents should
	 * be created with an initial ACTakeRole action to enter into the pre-created
	 * GridEnvironment and ACRequestneighbors action to set the list of the
	 * neighbors in the environment. We set just two neighbor cells in LIVE state.
	 * Then we must verify the cells in the neighborhood remain in their initial
	 * state.
	 * 
	 */
	@Test
	public void scenarioUpdateStateThroughTicks(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			int numOfRows = 3;
			int numOfColumns = 3;
			private GridEnvironment gridEnvironment;
			int tick = 1;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<MAXAgent>();

				gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
				agents.add(gridEnvironment);

				// Create plan
				final Plan<Cell> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<Cell>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),

								new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
										new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)),
								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
												BigDecimal.valueOf(2)),
										new ACCalculateNumberOfLiveNeighbors<>(Organization.GRID_ENVIRONMENT.toString(),
												RCell.class, null)),

								new ActionActivator<>(
										ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3),
												BigDecimal.valueOf(2)),
										new ACUpdateState<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class,
												null)));
					}
				};

				// Create agents
				for (int i = 0; i < (numOfRows * numOfColumns); i++) {
					Cell cell = new Cell(rawPlan);
					agents.add(cell);
				}
				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Schedule an action repeating at each tick for testing if the
				// GridEnvironment is always full.

				ActivationSchedule schedule0 = ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(1));
				schedule(new ActionActivator<>(schedule0, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						System.out.println("The grid at tick 1: ");
						// set cell 1x1 and 2x2 to alive state
						Cell[][] cells = gridEnvironment.getGrid();

						// all neighbors are made alive
						Cell cell = cells[0][0];
						GridContext context_ij = (GridContext) cell
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[0][1];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[0][2];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[1][1];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						cell = cells[2][2];
						context_ij = (GridContext) cell.getContext(Organization.GRID_ENVIRONMENT.toString());
						context_ij.setCellState(CellState.ALIVE);

						System.out.println(gridEnvironment.displayAsString());
					}

				}));

				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3),
						BigDecimal.valueOf(2));
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						tick++;
						System.out.println("The grid at tick " + tick + ":");
						System.out.println(gridEnvironment.displayAsString());
					}
				}));
			}
		};

		launchTester(tester, 20, testInfo);
	}

}
