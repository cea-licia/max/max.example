/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.env;

import static max.core.MAXParameters.setParameter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;

import madkit.gui.AgentFrame;
import madkit.kernel.AgentAddress;
import max.core.Context;
import max.core.IEventListener;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.model.cellular.life.action.Cell;
import max.model.cellular.life.action.CellState;
import max.model.cellular.life.action.IObservedCell;
import max.model.cellular.life.role.RCell;

public class GridEnvironment extends SimulatedEnvironment {

	private int numOfColumns; // number of columns
	private int numOfRows; // number of rows
	private Cell[][] grid; // a cellular grid
	private int pos_x; // row of the last agent added
	private int pos_y; // column of the last agent added

	public GridEnvironment(int numOfColumns, int numOfRows) {
		super();
		addAllowedRole(RCell.class);
		setName(Organization.GRID_ENVIRONMENT.toString());

		if (numOfColumns < 2 || numOfRows < 2) {
			throw new IllegalArgumentException("The grid must be at least 2x2");
		} else {
			this.numOfColumns = numOfColumns;
			this.numOfRows = numOfRows;
			grid = new Cell[numOfRows][numOfColumns];
		}
	}

	@Override
	public void register(AgentAddress address, IEventListener obj) {
		super.register(address, obj);

		String roleInAddress = address.getRole();
		String expectedRole = RCell.class.getCanonicalName();

		// add the agent to the grid cellular at position (pos_x, pos_y)
		if (roleInAddress.equals(expectedRole)) {
			Cell cell = (Cell) obj;
			grid[this.pos_x][this.pos_y] = cell;
			GridContext gridContext = (GridContext) cell.getContext(getName());
			gridContext.setPosition(this.pos_x, this.pos_y);
			if (this.pos_y < this.numOfColumns - 1) {
				this.pos_y++;
			} else if (this.pos_x < this.numOfRows - 1) {
				this.pos_x++;
				this.pos_y = 0;
			}
		}
	}

	public Cell[][] getGrid() {
		return grid;
	}

	public boolean isEmpty() {
		return getAgents().isEmpty();
	}

	public int getNumberOfCells() {
		return getAgentsWithRole(getName(), RCell.class).size();
	}

	@Override
	protected Context createContext(SimulatedAgent agent) {
		return new GridContext(agent, this);
	}

	private void addNeighbors(List<IObservedCell> neighbors, int x, int y) {
		if ((y >= 0 && y < this.numOfColumns && x >= 0 && x < this.numOfRows)) {
			Cell cell = grid[x][y];
			neighbors.add(cell);
		}
	}

	public List<IObservedCell> getNeighbors(int posX, int posY) {
		List<IObservedCell> neighbors = new ArrayList<IObservedCell>();
		Cell cell = grid[posX][posY];
		for (int i = posX - 1; i <= posX + 1; i++) {
			for (int j = posY - 1; j <= posY + 1; j++) {
				if (!(i == posX && j == posY)) { // Cell does not add itself as a neighbor
					addNeighbors(neighbors, i, j); // add the neighbor (if not out of the grid only)
				}
			}
		}
		return neighbors;
	}

	/*
	 * User Interface
	 */
	private JPanel sliderPanel = new JPanel(new BorderLayout());
	private JTable table;
	private JTextPane text;
	private JPanel pane = new JPanel();

	// display the grid as a string
	public String displayAsString() {
		String result = "";
		for (int i = 0; i < this.numOfRows; i++) {
			for (int j = 0; j < this.numOfColumns; j++) {
				Cell cell = grid[i][j];
				GridContext context = (GridContext) cell.getContext(getName());
				if (context.getCellState() == CellState.ALIVE) {
					result += "O";
				} else {
					result += "X";
				}
			}
			result += "\n";
		}
		return result;
	}

	// set up frame and add table and buttons required
	@Override
	public void setupFrame(AgentFrame frame) {
		super.setupFrame(frame);
		frame.setLocation(1000, 0);
		Optional.of(frame).ifPresent(f -> {
			super.setupFrame(f);
			// Remove MaDKit menu bar
			f.getRootPane().setJMenuBar(null);
			// Set a preferred size for the frame
			f.setPreferredSize(new Dimension(900, 850));
			// create frame
			f.setLayout(new BorderLayout());
			// add panel to frame
			frame.add(pane);
			// set title of frame
			frame.setTitle("Game Of Life");
			frame.setVisible(true);
			frame.pack();
		});
		// create the table
		table = new JTable(numOfRows, numOfColumns);
		table.setBackground(Color.BLACK);
		// pane.setAlignmentX(Component.LEFT_ALIGNMENT);
		int prefered_width = 12; //600/numOfColumns;
		System.out.println(prefered_width);
		// set the size of columns
		table.setRowHeight(prefered_width);
		for (int j = 0; j < this.numOfColumns; j++) {
			table.getColumnModel().getColumn(j).setPreferredWidth(prefered_width);
		}

		// create a Slider for density of live cells
		JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 100, 35);
		slider.setPreferredSize(new Dimension(200, 100));
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(new Integer(0), new JLabel("Density"));
		slider.setMinorTickSpacing(10);
		slider.setMajorTickSpacing(20);
		slider.setLabelTable(labelTable);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.addChangeListener((ChangeEvent event) -> {
			double value = (double) slider.getValue();
			setParameter("MAX_MODEL_CELLULAR_LIFE_INITIAL_DENSITY", String.valueOf(value / 100));
		});

		// create a button to set the live cells randomly
		JButton random = new JButton("Random");
		random.setBounds(new Rectangle(80, 6, 83, 26));
		random.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setParameter("MAX_MODEL_CELLULAR_LIFE_RANDOM_STATE", "true");
				setUpGrid();
			}
		});

		// add table, label and slider to the pane
		pane.add(table);
		pane.add(slider);
		pane.add(random);

	}

	private void setUpGrid() {
		for (int j = 0; j < this.numOfColumns; j++) {
			for (int i = 0; i < this.numOfRows; i++) {
				GridContext context = (GridContext) grid[i][j].getContext(getName());
				context.setUp();
			}
		}
	}

	public void updatePanel() {
		for (int j = 0; j < this.numOfColumns; j++) {
			for (int i = 0; i < this.numOfRows; i++) {
				table.getColumnModel().getColumn(j).setCellRenderer(new DefaultTableCellRenderer() {
					@Override
					public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
							boolean hasFocus, int row, int column) {
						DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(
								table, value, isSelected, hasFocus, row, column);
						GridContext context = (GridContext) grid[row][column]
								.getContext(Organization.GRID_ENVIRONMENT.toString());
						JLabel label = (JLabel) renderer;
						if (context.getCellState().equals(CellState.ALIVE)) {
							renderer.setBackground(Color.RED);
						} else {
							renderer.setBackground(Color.BLACK); // Restore original color
						}
						return renderer;
					}
				});
			}
		}
		table.repaint();
	}

}