/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.exp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import max.core.ExperimenterAction;
import max.core.MAXParameters;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationSchedule;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.cellular.life.action.ACCalculateNumberOfLiveNeighbors;
import max.model.cellular.life.action.ACRequestNeighbors;
import max.model.cellular.life.action.ACUpdateState;
import max.model.cellular.life.action.Cell;
import max.model.cellular.life.env.GridEnvironment;
import max.model.cellular.life.env.Organization;
import max.model.cellular.life.role.RCell;

public class GameOfLifeExperimenter extends ExperimenterAgent {
	
	private int numOfRows = MAXParameters.getIntegerParameter("MAX_MODEL_CELLULAR_NUMBER_OF_COLUMNS");
	private int numOfColumns = MAXParameters.getIntegerParameter("MAX_MODEL_CELLULAR_NUMBER_OF_ROWS");
	
	private GridEnvironment gridEnvironment;

	public GameOfLifeExperimenter() {
		super();
	}

	@Override
	protected List<MAXAgent> setupScenario() {
		System.out.println("setup");
		List<MAXAgent> agents = new ArrayList<MAXAgent>();

		gridEnvironment = new GridEnvironment(numOfRows, numOfColumns);
		agents.add(gridEnvironment);

		// Create plan
		final Plan<Cell> rawPlan = new Plan<>() {
			@Override
			public List<ActionActivator<Cell>> getInitialPlan() {
				// Each agent will have three Roles: RCEll, ACRequestNumberOfLiveneighbors and
				// ACUpdateState
				return Arrays.asList(
						new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
								new ACTakeRole<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),
						// execute each 2 ticks starting from tick 2
						new ActionActivator<>(ActivationScheduleFactory.createOneTime(BigDecimal.ONE),
								new ACRequestNeighbors<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)),
						new ActionActivator<>(
								ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(2),
										BigDecimal.valueOf(2)),
								new ACCalculateNumberOfLiveNeighbors<>(Organization.GRID_ENVIRONMENT.toString(),
										RCell.class, null)),
						// execute each 2 ticks starting from tick 3
						new ActionActivator<>(ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3),
								BigDecimal.valueOf(2)),
								new ACUpdateState<>(Organization.GRID_ENVIRONMENT.toString(), RCell.class, null)));
			}
		};

		// Create agents
		for (int i = 0; i < (numOfRows * numOfColumns); i++) {
			Cell cell = new Cell(rawPlan);
			agents.add(cell);
		}
		return agents;
	}

	@Override
	protected void setupExperimenter() {
		ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(3), 
				BigDecimal.valueOf(2));
		schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
			@Override
			public void execute() {
				gridEnvironment.updatePanel();
			}
		}));
	}

	@Override
	protected void activate() {
		super.activate();
		getScheduler().pauseSimulation();
	}

}
