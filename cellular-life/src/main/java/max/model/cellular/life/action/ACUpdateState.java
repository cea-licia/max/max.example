/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.action;

import max.core.action.Action;
import max.core.role.Role;
import max.model.cellular.life.env.GridContext;

public class ACUpdateState <A extends Cell> extends Action<A> {
	public ACUpdateState(String environment, Class<? extends Role> role, A owner, Object... inputs) {
		super(environment, role, owner, inputs);
	}

	@Override
	public void execute() {
		//The update of the cell state follows 4 rules: 
		// if the cell has less than 2 neighbours or more than 3 neighbours, it dies
		// if the cell has exactly three neighobours, it becomes alive
		// if the cell has 2 neighbours, it remain in its state
		Cell cell = (Cell) getOwner();
		GridContext context = (GridContext) cell.getContext(this.getEnvironment());
		int numOfLiveNeighbours = context.getNumberOfLiveNeighbors();
		if ( numOfLiveNeighbours < 2 || numOfLiveNeighbours > 3) {
			context.setCellState(CellState.DEAD);
		} else if ( numOfLiveNeighbours == 3) {
			context.setCellState(CellState.ALIVE);
		} 	
	}

	@Override
	public <T extends Action<A>> T copy() {
		return (T) new ACUpdateState(this.getEnvironment(), getRole(), getOwner());	
	}

}

