/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.action;

import java.awt.Color;

public class CellInfo {
    String display;
    char bOrF = ' ';
    Color color = Color.black;

    public CellInfo(String display)
    {
        this.display = display;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public void setBorF(char bOrF)
    {
        this.bOrF = bOrF;
    }

}
