/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.action;

import java.util.List;

import max.core.action.Plan;
import max.core.agent.SimulatedAgent;
import max.model.cellular.life.env.GridContext;
import max.model.cellular.life.env.GridEnvironment;
import max.model.cellular.life.env.Organization;
import max.model.cellular.life.role.RCell;

public class Cell extends SimulatedAgent implements IObservedCell {

	public Cell(Plan<? extends SimulatedAgent> plan) {
		super(plan);
		addPlayableRole(RCell.class);
	}

	@Override
	public CellState getCellState() {
		GridContext context = (GridContext) getContext(Organization.GRID_ENVIRONMENT.toString());
		return context.getCellState();
	}

}
