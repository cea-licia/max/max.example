/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.env;

import java.util.List;

import max.core.Context;
import max.core.MAXParameters;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.model.cellular.life.action.CellState;
import max.model.cellular.life.action.IObservedCell;

/**
 * 
 * @author CB266444
 *
 */
public class GridContext extends Context {

	private CellState state;
	private int posX;
	private int posY;
	private List<IObservedCell> neighbors;
	private int numberOfLiveNeighbors;
	boolean isRandom = MAXParameters.getBooleanParameter("MAX_MODEL_CELLULAR_LIFE_RANDOM_STATE");

	public GridContext(SimulatedAgent owner, SimulatedEnvironment simulatedEnvironment) {
		super(owner, simulatedEnvironment);
		// set the initial states of cell to not random
		if (!isRandom) {
			this.state = CellState.DEAD;
		} else {
			// set the state randomly
			// generate a random number between 0.0 and 1.0
			double random = Math.random();
			double initialDensity = MAXParameters.getDoubleParameter("MAX_MODEL_CELLULAR_LIFE_INITIAL_DENSITY");
			if (random < initialDensity) {
				this.state = CellState.ALIVE;
			} else {
				this.state = CellState.DEAD;
			}
		}
	}

	public void setUp() {
		// if not random, all cells are dead
		if (!isRandom) {
			this.state = CellState.DEAD;
		} else {
			// set the state randomly
			// generate a random number between 0.0 and 1.0
			double random = Math.random();
			double initialDensity = MAXParameters.getDoubleParameter("MAX_MODEL_CELLULAR_LIFE_INITIAL_DENSITY");
			if (random < initialDensity) {
				this.state = CellState.ALIVE;
			} else {
				this.state = CellState.DEAD;
			}
		}
	}

	public List<IObservedCell> getNeighbors() {
		return this.neighbors;
	}

	public void setNeighbors(List<IObservedCell> neighbors) {
		this.neighbors = neighbors;
	}

	public void setCellState(CellState state) {
		this.state = state;
	}
	
	public CellState getCellState() {
		return this.state;
	}
	
	public int getNumberOfLiveNeighbors() {
		return this.numberOfLiveNeighbors;
	}
	
	public void setNumberOfLiveNeighbors(int numberOfLiveNeighbors) {
		this.numberOfLiveNeighbors = numberOfLiveNeighbors;
	}

	public void setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
	}
	
	public int getPosX() {
		return this.posX;
	}

	public int getPosY() {
		return this.posY;
	}
}