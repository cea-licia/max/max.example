/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.cellular.life.action;

import java.util.List;

import max.core.action.Action;
import max.core.role.Role;
import max.model.cellular.life.env.GridContext;
import max.model.cellular.life.env.GridEnvironment;

public class ACRequestNeighbors<A extends Cell> extends Action<A> {
	public ACRequestNeighbors(String environment, Class<? extends Role> role, A owner, Object... inputs) {
		super(environment, role, owner, inputs);
	}

	@Override
	public void execute() {
		Cell cell = (Cell) getOwner();
		GridContext context = (GridContext) cell.getContext(this.getEnvironment());
		GridEnvironment env = (GridEnvironment) context.getEnvironment();
		int posX = context.getPosX();
		int posY = context.getPosY();
		List<IObservedCell> neighbors = env.getNeighbors(posX, posY);
		context.setNeighbors(neighbors);
	}

	@Override
	public <T extends Action<A>> T copy() {
		return (T) new ACRequestNeighbors(this.getEnvironment(), getRole(), getOwner());
	}

}