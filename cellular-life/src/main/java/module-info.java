module max.example.cellularlife {
	// Exported packages
	exports max.model.cellular.life.action;
	exports max.model.cellular.life.role;
	exports max.model.cellular.life.env;

	// Dependencies
	requires max.core;
	requires java.logging;
	requires java.desktop;

	// Even if types from these modules are exposed, customer modules will
	// have to explicitly require them.
	// This is done because max.test is not strictly required as it only contains
	// testing utils
	requires transitive org.junit.jupiter.api;
	requires java.management;
	requires madkit;
}
