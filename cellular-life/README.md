# max.model.cellular.life

#### Game of life Model

The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.
It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input.
 One interacts with the Game of Life by creating an initial configuration and observing how it evolves.

![Demo](./src/gif/GIF 16-07-2021 13-12-39.gif) 

You can see every pattern in [here](https://troytae.github.io/game-of-life/).

## Overview

The Game of Life simulates a 2D-dimensional grid of square cells.
Each cell in the grid is an individual agent and can exist in a finite number of states.
Each cell is in one of two states, alive or dead.
The state of a cell depends on the states of its neighbors according to four rules.
As time progresses, each cell will reevaluate its current state and make any required changes.

## Entities, Roles and Behaviors

The model is composed of a `GridEnvironment`, `GridContext`, `IObservedCells` and `Cells`.

### GridEnvironment

`GridEnvironment` extends `SimulatedEnvironment` and constructs environment for the game of life, register the `Cell` agents and set up the visualization of the grid.
It also enables counting the number of live neighbors of each agent cell.

### GridContext 

`GridContext` extends `Context` and enables the cell to update its state according to three defined actions:

*  `ACRequestNeighbors`: is executed at tick one to register the neighbors of each cell in its context.
*  `ACCalculateNumberOfLiveNeighbours`: count the number of live neighbors considering the state of the IObservedCells.
*  `ACUpdateState`: change the cell state according to four rules:	
- Any living cell with fewer than two live neighbors dies (underpopulation).
- Any living cell with four or more neighbors die (overpopulation).
- Any cell with exactly two neighbors remain in its state.
- Any cell with exactly three neighbors become/remain alive. 
		
### Cell

`Cell` extends `SimulatedAgent` and plays RCell role.
Each agent in the grid has a `RCell` role.

###IObservedCells

`IObservedCells` is a cell interface that enables other agents to observe the cell state without accessing to its context.

### GameOfLifeExperimenter

`GameOfLifeExperimenter` plays the `RExperimenter` role.
It initializes a grid of size 50x50 and enables the agent cells with `RCell` role and `ACRequestNeighbors`, `ACCalculateNumberOfLiveNeighbors` and `ACUpdateState` actions.
It is responsible for running a simulation and initializing cell behaviors randomly and with a certain density.

The life cycle of this experimentation is as follows:
- At tick one, the scheduler requests the neighbors of each cell agent,
- At tick two, and after each two ticks, the scheduler calculate the number of live neighbors by observing their state,
- At tick three and after each two ticks, the scheduler  update the cell agent state according the initial state and to states progress.

This experience is launched during `1000` ticks. 
A visualization is provided to see the update states throw ticks.
 
#### Launch Experimentation     

`GridEnvironmentTest` conducts many tests and experiences to check the validity of the implementation.
To launch the `GameOfLifeExperimenter` using Maven, you should create an Eclipse configuration with the following goal:

```
compile exec:java -Dexec.mainClass="max.core.ExperimenterRunner" -Dexec.args="-experimenter defaultPropreties.xml"
```
To set up the experimentation parameters, you could change the numbers of rows and columns by default in `src/main/resources/defaultPropreties.xml`.

Once launched, a slider is provided to fix the density of live cells at the beginning of the experience.
The "random" button set the state cells randomly.
To start the experience, you should start the schedule agent.
Once started, the cells change colors depending on the state update:
	- red for live cells
	- black for dead cells
	
### Parameterization

*  max.core
	*  `MAX_CORE_SIMULATION_STEP` (1): BigDecimal parameter for defining the step size for the simulation experiment. It is important to note that if an agent tries to schedule a behavior for a time which is not a multiple of this parameter, this behavior will not be scheduled.
	*  `MAX_CORE_TIMEOUT_TICK` : BigDecimal parameter for the defining the number of ticks to launch the experimentation.
	*  `MAX_CORE_UI_MODE`: Boolean to provide the visualization.

*  max.model.cellular.life
	*  `MAX_MODEL_CELLULAR_LIFE_RANDOM_STATE` : Boolean to initialize the cells states randomly.
	*  `MAX_MODEL_CELLULAR_LIFE_INITIAL_DENSITY` : Double to initialize the density of live cells.
	*  `MAX_MODEL_CELLULAR_NUMBER_OF_ROWS`: Integer to initialize the number of rows of the grid.
	*  `MAX_MODEL_CELLULAR_NUMBER_OF_COLUMNS`: Integer to initialize the number of columns of the grid.
